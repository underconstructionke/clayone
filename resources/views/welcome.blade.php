<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Raphael Otaalo</title>

    <link href="https://fonts.googleapis.com/css?family=Raleway%7CMontserrat:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/fontawesome-5.2.0/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/stylesheet.css') }}">  <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset('assets/css/media.css') }}">  <!-- Media Queries -->
    <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script> <!-- Adding jQuery Library -->
</head>
<body>

<!-- LOADER -->

<div id="loader">
    <div class="loading"></div>
</div>

<!-- LOADER -->


<!-- HEADER -->

<!-- Header Section -->
<div id="header">
    <!-- Wrapper -->
    <div class="wrapper">
        <!-- Logo -->
        <div class="logo">
            <a href="#"><img src="#" alt="logo"></a>
        </div>
        <!-- Menu -->
        <div class="menu">
            <ul>
                <li><a href="#hero" class="active">Home</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#service">Services</a></li>
                <li><a href="#blog">Blog</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="javascript:void(0);" class="buy_now"><i class="fas fa-shopping-cart"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<!-- HEADER -->


<!-- Mobile Menu -->

<div id="mobile_menu">
    <div class="head_wrap">
        <div class="mobile_header">
            <div class="logo">
                <a href="#"><img src="#" alt="logo"></a>
            </div>
            <div class="menu">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    </div>
    <div class="link_wrap">
        <div class="mobile_links">
            <ul>
                <li><a href="#hero">Home</a></li>
                <li><a href="#portfolio">Portfolio</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#service">Services</a></li>
                <li><a href="#blog">Blog</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- Mobile Menu -->


<!-- HERO -->

<!-- Hero section which includes background as solid_color, video, and image at different pages -->
<div id="hero">
    <!-- Hero Intro Text -->
    <div class="intro_text">
        <h1>Hello, I'm <span class="highlight">Raphael.</span></h1>
        <p>A creative <span class="highlight">designer</span> and <span class="highlight">front-end developer</span>
            based in Kenya.</p>
        <!-- Read More -->
        <div class="more_about_me">
            <a href="javascript:void(0)"><span>About Me</span></a>
        </div>
    </div>
    <!-- Background as solid color. To change background color edit stylesheet.css -->
    <div class="hero_background_solid"></div>
</div>

<!-- HERO -->


<!-- Portfolio -->

<div id="portfolio">
    <!-- Portfolio Title-->
    <div class="portfolio_title">
        <h1>Portfolio.</h1>
        <p>Please check some of mine recent projects below. I do hope you will be inspired .</p>
    </div>
    <!-- Portfolio Container -->
    <div class="portfolio_container">
        <!-- Portfolio 1 -->
        <div class="p1">
            <div class="p1_overlay">
                <h3>Photography</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/1.jpg')  }}" alt="photo">
        </div>
        <!-- Portfolio 2 -->
        <div class="p2">
            <div class="p2_overlay">
                <h3>Product Branding</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/2.jpg')  }}" alt="branding">
        </div>
        <!-- Portfolio 3 -->
        <div class="p3">
            <div class="p3_overlay">
                <h3>Designing</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/3.jpg')  }}" alt="designing">
        </div>
        <!-- Portfolio 4 -->
        <div class="p4">
            <div class="p4_overlay">
                <h3>Application</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/4.jpg')  }}" alt="graphic">
        </div>
        <!-- Portfolio 5 -->
        <div class="p5">
            <div class="p5_overlay">
                <h3>Website</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/5.jpg')  }}" alt="website">
        </div>
        <!-- Portfolio 6 -->
        <div class="p6">
            <div class="p6_overlay">
                <h3>Branding</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/6.jpg')  }}" alt="branding">
        </div>
        <!-- Portfolio 7 -->
        <div class="p7">
            <div class="p7_overlay">
                <h3>Graphic</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/7.jpg')  }}" alt="apps">
        </div>
        <!-- Portfolio 8 -->
        <div class="p8">
            <div class="p8_overlay">
                <h3>Designing</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/8.jpg')  }}" alt="design">
        </div>
        <!-- Portfolio 9 -->
        <div class="p9">
            <div class="p9_overlay">
                <h3>Photography</h3>
            </div>
            <img src="{{ asset('assets/images/portfolio/9.jpg')  }}" alt="photo">
        </div>

    </div>

    <!-- Portfolio Load More -->
    <div class="portfolio_load_more">
        <a href="#"><span>Load More</span></a>
    </div>

</div>

<!-- PORTFOLIO -->


<!-- CASESTUDY -->

<!-- Casestudy Section -->
<div id="casestudy">

    <!-- Casestudy Container 1 -->
    <div class="case_container_1">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project One.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>

    <!-- End Casestudy Container 1-->

    <!-- Casestudy Container 2-->

    <div class="case_container_2">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Two.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>
    <!-- End Casestudy Container 2-->


    <!-- Casestudy Container 3-->

    <div class="case_container_3">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Three.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>
    <!-- End Casestudy Container 3-->

    <!-- Casestudy Container 4-->

    <div class="case_container_4">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Four.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>
    <!-- End Casestudy Container 4-->

    <!-- Casestudy Container 5-->

    <div class="case_container_5">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Five.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>
    <!-- End Casestudy Container 5-->

    <!-- Casestudy Container 6-->

    <div class="case_container_6">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Six.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>
    <!-- End Casestudy Container 6-->

    <!-- Casestudy Container 7-->

    <div class="case_container_7">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Seven.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>
    <!-- End Casestudy Container 7-->

    <!-- Casestudy Container 8-->

    <div class="case_container_8">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Eight.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>

    <!-- End Casestudy Container 8-->

    <!-- Casestudy Container 9-->

    <div class="case_container_9">
        <div class="close">
            <span>close</span>
        </div>
        <div class="case_img1">
            <img src="{{ asset('assets/images/casestudy/1.jpg')  }}" alt="casestudy">
        </div>
        <div class="case_img2">
            <img src="{{ asset('assets/images/casestudy/2.jpg')  }}" alt="work">
        </div>
        <div class="case_img3">
            <img src="{{ asset('assets/images/casestudy/3.jpg')  }}" alt="product Image">
        </div>
        <!-- Project Details -->
        <div class="project">
            <h1>Project Nine.</h1>
            <div class="description">
                <p>Quisque molestie tincidunt interdum. Vivamus sed pulvinar enim. Praesent sit amet nulla pellentesque
                    tellus porttitor cursus. Aliquam congue tincidunt neque, vitae mollis ex feugiat sed. Donec iaculis
                    lorem velit, et dignissim nulla feugiat ac. Morbi efficitur nec risus vitae vestibulum.</p>
                <p>Aenean elementum nulla massa, in vestibulum metus convallis eget. Mauris lobortis mollis purus,
                    luctus convallis tortor eleifend ac. Phasellus semper ipsum sit amet odio rhoncus tempor. Aliquam
                    congue tincidunt neque, vitae mollis ex feugiat sed.</p>
            </div>
            <!-- Client Details, skill used, link to visit website -->
            <div class="details">
                <h3>Client - </h3>
                <span> Designing Studio. </span>
                <h3>Role - </h3>
                <span> UI/UX resdesigning, Branding, making website responsive. </span>
                <h3>Visit - </h3>
                <a href="http://preview.themeforest.net/item/curious-onepage-parallax/full_screen_preview/20868518"
                   target="_blank"><span> www.curioustheme.tk </span></a>
            </div>
        </div>
    </div>
    <!-- End Casestudy Container 9-->

</div>

<!-- CASESTUDY -->


<!-- ABOUT -->

<div id="about">
    <!-- About Container -->
    <div class="about_container">
        <!-- About Me Content -->
        <div class="about_me">
            <h1>About Me.</h1>
            <h3>I’m very pragmatic but more of a principled guy and I only mingle with those who make sense to me.</h3>
            <p>I'm a celebrated winner of the prestigious American Talent Wave Awards and one of the top young
                bestselling authors in the U.S.A. I have captured the attention of the American literary icons like
                Charles Dickens, Sidney Sheldon, Ben Carson and Hillary Clinton who have positively commented on my
                books. And I have appeared severally in the New York Times, the Washington Post and the British Times
                newspapers. </p>
            <!-- Resume Button -->
            <div class="resume">
                <a href="javascript:void(0)"><span>Resume</span></a>
            </div>
            <!-- Contact Me Button -->
            <div class="contact_me">
                <a href="javascript:void(0)"><span>Contact</span></a>
            </div>
        </div>
    </div>

    <!--My Image-->
    <div class="my_image">
        <img src="https://clayceelblog.files.wordpress.com/2016/04/on-a-fashion-exhibition1.jpg?w=215" alt="my_image">
    </div>

</div>

<!-- ABOUT -->


<!-- EDUCATION -->

<div id="education">
    <!-- Education Title -->
    <div class="education_title">
        <h1>Education and Experience.</h1>
        <p>This are my academic achievements and field experience.</p>
    </div>

    <!-- Education Container -->
    <div class="education_container">
        <div class="e2">
            <div class="e2_details">
                <h3>Kakamega Primary School</h3>
                <span>Yr - 2006 to 2008</span>
                <p>KCPE: 500</p>
            </div>
        </div>
        <!-- Education 1 -->
        <div class="e1">
            <div class="e1_details">
                <h3>Kakamega Sec. School</h3>
                <span>Yr - 2005 to 2008</span>
                <p>KCSE A+</p>
            </div>
        </div>
        <!-- Education 2 -->

        <!-- Education 3 -->
        <div class="e3">
            <div class="e3_details">
                <h3> University of Birmingham</h3>
                <span>Yr - 2011 to 2012 (Internship)</span>
                <p>Business English</p>
            </div>
        </div>


    </div>
</div>

<!-- EDUCATION -->


<!-- HIRE ME -->

<div id="hire">
    <!-- Hire Me Container -->
    <div class="hire_container">
        <!-- Hire Me Text -->
        <div class="hire_me_content">
            <h1>Hope you like my portfolio. Let's connect and create wonderful stuffs together.</h1>
        </div>
        <!-- Hire Me Button -->
        <div class="hire_me_button">
            <a href="mailto:'connect@curioustheme.tk'"><span>Hire Me</span></a>
        </div>
    </div>
    <img src="{{ asset('assets/images/background/2.jpg')  }}" alt="hire me">
</div>

<!-- HIRE ME -->


<!-- SERVICES -->

<div id="service">
    <!-- Service Title -->
    <div class="service_title">
        <h1>Services.</h1>
        <p>My skills and services through which I could serve you.</p>
    </div>
    <!-- Service Container -->
    <div class="service_container">
        <!-- Service 1 -->
        <div class="s1">
            <div class="s1_details">
                <i class="fas fa-camera-retro"></i>
                <h3>Photography</h3>
            </div>
        </div>
        <!-- Service 2 -->
        <div class="s2">
            <div class="s2_details">
                <i class="fas fa-bullhorn"></i>
                <h3>Public speaker</h3>
            </div>
        </div>
        <!-- Service 3 -->
        <div class="s3">
            <div class="s3_details">
                <i class="fas fa-book"></i>
                <h3>Web Development</h3>
            </div>
        </div>
        <!-- Service 4 -->
        <div class="s4">
            <div class="s4_details">
                <i class="fas fa-file-code"></i>
                <h3>project proposals</h3>
            </div>
        </div>
        <!-- Service 5 -->
        <div class="s5">
            <div class="s5_details">
                <i class="fas fa-book"></i>
                <h3>Magazine Design and Copy Writing</h3>
            </div>
        </div>
        <!-- Service 6 -->
        <div class="s6">
            <div class="s6_details">
                <i class="fas fa-file-contract"></i>
                <h3>Business plans</h3>
            </div>
        </div>

    </div>
</div>

<!-- SERVICES -->


<!-- FACTS -->

<div id="facts">
    <!-- Fact Container -->
    <div class="fact_container">
        <!-- Fact 1 -->
        <div class="f1">
            <i class="fas fa-heart"></i>
            <h3>1032</h3>
            <p>Happy Clients</p>
        </div>
        <!-- Fact 2 -->
        <div class="f2">
            <i class="fas fa-paper-plane"></i>
            <h3>1150</h3>
            <p>Project Completed</p>
        </div>
        <!-- Fact 3 -->
        <div class="f3">
            <i class="fas fa-trophy"></i>
            <h3>32</h3>
            <p>Award Received</p>
        </div>
        <!-- Fact 4 -->
        <div class="f4">
            <i class="fas fa-coffee"></i>
            <h3>1232</h3>
            <p>Coffee Drank</p>
        </div>
    </div>
    <div class="background_img">
        <img src="{{ asset('assets/images/background/3.jpg')  }}" alt="background">
    </div>
</div>

<!-- FACTS -->


<!-- BLOG -->

<div id="blog">
    <!-- Blog Title -->
    <div class="blog_title">
        <h1>My Blog.</h1>
        <p>Hey I even do blogging of my profession, travel and such.</p>
    </div>
    <!-- Blog Container -->
    <div class="blog_container">
        <!-- Blog 1 -->
        <div class="b1">
            <a href="javascript:void(0)">
                <div class="b1_img">
                    <img src="{{ asset('assets/images/blog/1.jpg')  }}" alt="tutorial">
                </div>
                <div class="b1_txt">
                    <span>10.05.2018</span>
                    <h3>Tips On How To Secure A Job</h3>
                    <p>When writing an application letter for any particular job, tailor a striking level of formality
                        that conforms to the job that you are applying for. Make your letter simple, clear and
                        grammatically polished</p>
                </div>
            </a>
        </div>
        <!-- Blog 2 -->
        <div class="b2">
            <a href="javascript:void(0)">
                <div class="b2_img">
                    <img src="{{ asset('assets/images/blog/2.jpg')  }}" alt="website">
                </div>
                <div class="b2_txt">
                    <span>16.06.2018</span>
                    <h3>A Young Emblem of Greatness</h3>
                    <p>Despite being young, Clay Ceel is a great talented African author of the recent time who embraces
                        Modern Literature with an outstanding and mature literary perception of life.</p>
                </div>
            </a>
        </div>
        <!-- Blog 3 -->
        <div class="b3">
            <a href="javascript:void(0)">
                <div class="b3_img">
                    <img src="{{ asset('assets/images/blog/3.jpg')  }}" alt="digital">
                </div>
                <div class="b3_txt">
                    <span>21.07.2018</span>
                    <h3>A Great Young Talented Mind</h3>
                    <p>I read Clay Ceel’s book at the New York-based Library and I was so amazed to read such a great
                        and well-crafted inspirational book written by this young man from Kenya. Clay Ceel will
                        definitely go far as an author.</p>
                </div>
            </a>
        </div>

    </div>
</div>

<!-- BLOG -->


<!-- CLIENTS -->

<div id="client">
    <!-- Client Container -->
    <div class="client_container">
        <div class="c1"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
        <div class="c2"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
        <div class="c3"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
        <div class="c4"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
        <div class="c5"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
        <div class="c6"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
        <div class="c7"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
        <div class="c8"><img src="{{ asset('assets/images/client_logo/1.png') }}" alt="client logo"></div>
    </div>
</div>

<!-- CLIENTS -->


<!-- CONTACT -->

<div id="contact">
    <!-- Contact Title -->
    <div class="contact_title">
        <h1>Contact Me.</h1>
        <p>You can easily connect with me using <span class="highlight">contact form</span> below. </p>
    </div>
    <!-- Contact Container -->
    <div class="contact_container">
        <!-- Form Section -->
        <div class="form">
            <form method="post">
                <input type="text" name="name" class="name" placeholder="Name">
                <input type="email" name="email" class="email" placeholder="Email">
                <textarea class="message" name="message" placeholder="Message"></textarea>
                <input type="submit" class="submit" name="submit" value="submit">
            </form>
            <!-- Form Error -->
            <div class="form_message">
                <span class="error">Please enter your details in all fields.</span>
                <span class="success">Thank you for connecting, I will revert you soon.</span>
            </div>
        </div>
        <!-- Address and Other Details section -->
        <div class="details">
            <div class="details_wrap">
                <h3>Hey, you can even connect with me using information provided below.</h3>
                <div class="address">
                    <h3>Address -</h3>
                    <p>Mahali, Kakamega.</p>
                </div>
                <div class="phone">
                    <h3>Phone -</h3>
                    <p>(+254) 713 642 178</p>
                </div>
                <div class="email">
                    <h3>Email -</h3>
                    <a href="mailto:'info@raphaelotaalo.co.ke'">info@raphaelotaalo.co.ke</a>
                </div>
            </div>
            <i class="fas fa-map-marker-alt"></i>
        </div>

    </div>
</div>

<!-- CONTACT -->


<!-- FOOTER -->

<div id="footer">
    <!-- Footer Container -->
    <div class="footer_container">
        <div class="copyright">
            <span>Raphael Otaalo</span>
            <span>All right reserved &copy; {{ date('Y') }}.</span>
        </div>
        <!-- Social Icons -->
        <div class="social">
            <ul>
                <li><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <li><a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="https://www.behance.net/" target="_blank"><i class="fab fa-behance"></i></a></li>
                <li><a href="https://dribbble.com/" target="_blank"><i class="fab fa-dribbble"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<!-- FOOTER -->


<!-- Click to Scroll -->

<div id="scroll_top">
    <i class="fas fa-angle-up"></i>
</div>

<!-- Click to Scroll -->


<!-- JavaScript and jQuery -->

<script src="{{ asset('assets/js/loader.js') }}"></script>
<script src="{{ asset('assets/js/mobile_menu.js') }}"></script>
<script src="{{ asset('assets/js/click_menu.js') }}"></script>
<script src="{{ asset('assets/js/scroll_menu.js') }}"></script>
<script src="{{ asset('assets/js/hero_text.js') }}"></script>
<script src="{{ asset('assets/js/portfolio.js') }}"></script>
<script src="{{ asset('assets/js/counter.js') }}"></script>
<script src="{{ asset('assets/js/form.js') }}"></script>
<script src="{{ asset('assets/js/scroll_top.js') }}"></script>

<!-- JavaScript and jQuery -->

</body>
</html>
